document.getElementById("submit").addEventListener("click", e => {
    e.preventDefault();
    let invalid = false;
    const result = document.getElementById("result");

    const genders = document.getElementsByName("gender");
    let gender;
    genders.forEach(radio => {
        if (radio.checked) gender = radio.value;    
    });
    
    const weight = Number(document.getElementById("weight").value);
    const timeSince = Number(document.getElementById("timeSince").value);
    const size = document.getElementById("size").value;
    const volume = Number(document.getElementById("volume").value);
    const doses = Number(document.getElementById("doses").value);

    if (!gender || !weight || !timeSince || !doses) {
        invalid = true;
        result.innerHTML = "Invalid";
    }

    if (!invalid) {
        const litres = size * doses;
        const grams = litres * 8 * volume;
        const burning = weight / 10;
        const left = grams - (burning * timeSince);
        const bac = gender === "male" ? left / (weight * 0.7) : left / (weight * 0.6);

        result.innerHTML = `Your BAC is ${bac.toFixed(2)}`;
    }    
});

document.getElementById("reset").addEventListener("click", e => {
    e.preventDefault();
    document.getElementById("result").innerHTML = "Not calculated";
    const genders = document.getElementsByName("gender");
    genders.forEach(radio => {
        radio.checked = false;    
    });
    document.getElementById("weight").value = "";
    document.getElementById("timeSince").value = "";
    document.getElementById("size").value = "0.04";
    document.getElementById("volume").value = "";
    document.getElementById("doses").value = "";
});