import axios from "axios";

const [{ data : users}, {data : todos}] = await Promise.all([axios.get("https://jsonplaceholder.typicode.com/users/"), axios.get("https://jsonplaceholder.typicode.com/todos/")]);

const combined = todos.map(todo => {
    const { ...obj } = todo;

    const user = users.find(user => {
        return user.id === obj.userId;
    });

    obj.user = { name: user.name, username: user.username, email: user.email };
    return obj;
});

console.log(combined);