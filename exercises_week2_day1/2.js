const num1 = Number(process.argv[2]);
const num2 = Number(process.argv[3]);
const password = process.argv[4];

if (num1 > num2) {
	console.log(`${num1} is greater than ${num2}`);
} else if (num1 < num2) {
	console.log(`${num2} is greater than ${num1}`);
} else {
	console.log("They are equal");
	if (password === "hello world") {
		console.log("yay, you guessed the password");
	}
}