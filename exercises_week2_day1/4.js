const month = Number(process.argv[2]);
const days = new Date(2022, month, 0).getDate();

console.log(`${days} days in month ${month}`);