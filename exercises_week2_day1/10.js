const charToBeReplaced = process.argv[2];
const replacementChar = process.argv[3];
const string = process.argv[4];

const newString = string.replace(new RegExp(charToBeReplaced, "g"), replacementChar);

console.log(newString);