//En huomaa, miten tämän voisi tehdä pelkillä konditionaaleilla ilman pitkiä if elsejä.
const number_1 = Number(process.argv[2]);
const number_2 = Number(process.argv[3]);
const number_3 = Number(process.argv[4]);
const numbers = [number_1, number_2, number_3];

let numberObjects = numbers.map((number, index) => {
    return {value: number, name: "number_" + index};
});

numberObjects.sort((a, b) => a.value - b.value);

if (numberObjects[0].value === numberObjects[2].value) {
    console.log("they are all equal");
} else {
    console.log(`Largest: ${numberObjects[2].name}, ${numberObjects[2].value} 
Smallest: ${numberObjects[0].name}, ${numberObjects[0].value}`);
}