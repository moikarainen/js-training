(() => {
    let balance = 0;
    let isActive = true;
    let checkbalance = true;

    if (!checkbalance) {
        return console.log("Have a nice day");
    }

    if (balance > 0 && isActive) {
        return console.log(`Balance: ${balance}`);
    }

    if (!isActive) {
        return console.log("Your account is not active");
    }

    if (balance === 0) {
        return console.log("Your account is empty");
    }

    return console.log("Your balance is negative")
})();