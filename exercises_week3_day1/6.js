const generateCredentials = (firstName, lastName) => {
    const year = new Date().getFullYear();
    const userName = `B${year.toString()[2]}${year.toString()[3]}${firstName[0].toLowerCase()}${firstName[1].toLowerCase()}${lastName[0].toLowerCase()}${lastName[1].toLowerCase()}`;
    const password = `${String.fromCharCode(Math.random() * (90 - 65) + 65)}${firstName[0].toLowerCase()}${lastName[lastName.length - 1].toUpperCase()}${String.fromCharCode(Math.random() * (47 - 33) + 33)}${year.toString()[2]}${year.toString()[3]}`;

    console.log(`Username: ${userName} Password: ${password}`);
};

generateCredentials("John", "Doe");