const getVowelCount = (string) => {
    const array = string.split("");
    const vowelCount = array.filter(element => ["a", "e", "i", "o", "u", "y"].includes(element)).length;

    return vowelCount;
};

console.log(getVowelCount('abracadabra'));