const wrapper = (n) => {
    let steps = 0;

    const recur = (n) => {
        if (n === 1) {
            return steps;
        } else if (n % 2 === 0) {
            steps++
            recur(n/2);        
        } else {
            steps++
            recur(n*3 + 1); 
        }
    };

    recur(n);

    return steps;
};

console.log(wrapper(3));