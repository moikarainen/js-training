const like = (likes) => {
    if (likes.length === 1) {
        return console.log(`${likes[0]} likes this`);
    }

    if (likes.length < 3) {
        return console.log(`${likes[0]} and ${likes[1]} like this`);
    }

    const rest = likes.slice(2);
    
    console.log(`${likes[0]}, ${likes[1]} and ${rest.length} others like this`);
};

like(["asdf"]);

like(["asdf", "fad"]);

like(["asdf","fda", "gfdsa", "gseara"]);