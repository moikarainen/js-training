const checkExam = (correct, submitted) => {
    let points = [];

    for (let i = 0; i < submitted.length; i++) {
        if (submitted[i] === correct[i]) {
            points.push(4);
        } else if (submitted[i] === "") {
            points.push(0);
        }   else {
            points.push(-1);
        }
    }

    const score = points.reduce((a, b) => a + b);

    return score >= 0 ? score : 0;
}

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])); //→ 6
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""])); //→ 7
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"])); //→ 16
console.log(checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"])); //→ 0
