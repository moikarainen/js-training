import { useState } from 'react';
import './App.css';

const Task = ({ task, removeTask }) => {
  return (
    <div className='task-container'>
      <div className='task'>
        <p><b>{task}</b></p>
      </div>
      <div className='remove'>
        <button onClick={removeTask}><b>Remove</b></button>
      </div>
    </div>
  );
};

function App() {
  const [tasks, setTasks] = useState([]);
  const [task, setTask] = useState("");

  const submitTask = () => {
    if (task) {
      setTasks([...tasks, task]);
      setTask("");
    }
  };

  const handleChange = (e) => {
    setTask(e.target.value);
  };
  
  const removeTask = (taskToRemove) => {
    setTasks(tasks.filter(task => task !== taskToRemove));
  };

  return (
    <div className="App">
      <div className="header">
        <input type="text" value={task} onChange={handleChange}></input>
        <button onClick={submitTask}><b>Add</b></button>
      </div>
      <div>
        {tasks.map(task => <Task key={task} task={task} removeTask={() => {removeTask(task)}}/>)}
      </div>
    </div>
  );
}

export default App;
