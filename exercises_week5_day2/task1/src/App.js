const Title = ( { titleText } ) => {
  return (
    <h1>{titleText}</h1>
  );
}

const EulerPicture = ( { url, caption } ) => {
  return (
    <div>
      <img src={url} alt="#"></img>
      <figcaption>{caption}</figcaption>
    </div>
  );
}

const TextContent = ( {text, source} ) => {
  return (
    <div>
      <p>{text}</p>
      <a href={source}>Source</a>
    </div>
  );
}

function App() {
  const titleText = "Shrine to Leonhard Euler";
  const url = "../img/Euler.jpg";
  const caption = "the Man himself";
  const text = "Leonhard Euler (15 April 1707 - 18 September 1783) was a Swiss mathematician, physicist, astronomer, geographer, logician and engineer who founded the studies of graph theory and topology and made pioneering and influential discoveries in many other branches of mathematics such as analytic number theory, complex analysis, and infinitesimal calculus. He introduced much of modern mathematical terminology and notation, including the notion of a mathematical function.[3] He is also known for his work in mechanics, fluid dynamics, optics, astronomy and music theory.";
  const source = "https://en.wikipedia.org/wiki/Leonhard_Euler";

  return (
    <div>
      <Title titleText={titleText}/>
      <EulerPicture url={url} caption={caption}/>
      <TextContent text={text} source={source}/>
    </div>
  );
}

export default App;
