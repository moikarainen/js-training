const Singles = () => {
    const singles = [
        { title: "Du hast", album: "Sehnsucht", year: 1997 },
        { title: "Ich will", album: "Mutter", year: 2001 },
        { title: "Feuer frei", album: "Mutter", year: 2002 },
        { title: "Amerika", album: "Reise, Reise", year: 2004 },
        { title: "Mann gegen Mann", album: "Rosenrot", year: 2005 },
        { title: "Deutschland", album: "Untitled", year: 2019 },
        { title: "Zick Zack", album: "Zeit", year: 2022 },
    ];

    const output = singles.map(single => {
        return (
        <div>
            <p><b>Title: {single.title}</b></p>
            <p>Album: {single.album}</p>
            <p>Year: {single.year}</p>
        </div>
        );
    });

    return (
        <div>
            {output}
        </div>
    );
}

export default Singles;