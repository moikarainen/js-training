import './App.css';
import Output from './Output';
import { useState } from "react";

function App() {
  const [input, setInput] = useState("");

  const handleInput = (event) => {
    setInput(event.target.value);
  };

  return (
    <div>
      <input type="text" onChange={handleInput}/>
      <Output input={input}/>
    </div>
  );
}

export default App;
