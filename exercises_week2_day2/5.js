const string = process.argv[2];
const array = string.split(" ");
let newArray = [];
let reversedString;

for (let string in array) {
    let reversed = array[string].split("").reverse().join("");
    newArray.push(reversed);
}

reversedString = newArray.join(" ");

console.log(reversedString);