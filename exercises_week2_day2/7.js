const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
let suffledArray = [];

for (let i = 0; i < array.length; i++) {
    suffledArray[i] = array[i];
}

for (let i = 0; i < suffledArray.length; i++) {
    let randomIndex = Math.floor(Math.random() * (suffledArray.length - 1));
    [suffledArray[i], suffledArray[randomIndex]] = [suffledArray[randomIndex], suffledArray[i]];
}

console.log(suffledArray);