const string = process.argv[2];
const reversedString = string.split("").reverse().join("");

if (string === reversedString) {
    console.log(`Yes, ${string} is a palindrome`);
} else {
    console.log(`No, ${string} is not a palindrome`)
}