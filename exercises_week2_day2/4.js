const start = Number(process.argv[2]);
const end = Number(process.argv[3]);
const desc = start > end ? true : false;
let array = [];

for (let i = start; desc ? i >= end : i <= end; desc ? i-- : i++) {
    array.push(i);
}

console.log(array);