const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"]; const ordinals = ['st', 'nd', 'rd', 'th'];
let placements = [];

for (let i = 1; i < competitors.length; i++) {
    let ordinal = i - 1 <3 ? ordinals[i - 1] : ordinals[3];
    let string = `${competitors[i - 1]} was ${i}${ordinal}`;
    placements.push(string);
}

console.log(placements);