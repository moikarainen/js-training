import fs from "fs";

export const loadData = () => {
    try {
        const data = fs.readFileSync("data.json");
        return JSON.parse(data);
    } catch (error) {
        console.log(error);
    }
};

export const saveData = (data) => {
    try {
        fs.writeFileSync("data.json", data)
    } catch (error) {
        console.log(error);
    }
};

//validateId

//checkPassword