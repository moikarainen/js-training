import readline from "readline-sync";
import { loadData, saveData } from "./file.js";

let logged;

//toisto apufunktioihin

const commands = {

    help: () => {
        console.log("I'm glad to help you Here's a list of commands you can use!");
        console.log(`Accounts
    createAccount -- > Opens dialog for creating an account.
    closeAccount -- > Opens a dialog for closing an account.
    modifyAccount -- > Opens a dialog for modifying an account.
    doesAccountExist -- > Opens a dialog for checking if the account exists.
    login -- > Opens a dialog for logging in.
    logout -- > Opens a dialog for logging out.
        
    Funds
    withdrawFunds -- > Opens a dialog for withdrawing funds.
    depositFunds -- > Opens a dialog for depositing funds.
    transferFunds -- > Opens a dialog for transferring funds to another account.
    Requests
    requestFunds -- > Opens a dialog for requesting another user for funds.
    fundRequests -- > Shows all the requests for the account funds.
    acceptFundRequest -- > Opens a dialog for accepting a fund request.`);
    },

    createAccount: () => {
        console.log("So you want to create a new account!");

        let running = true;
        let name = "";
        let balance = 0;

        while (running) {

            while (!name) {
                name = readline.question("Let's start with the easy question. What is your name?").trim();
            }

            console.log(`Hey ${name}! It's create to have you as a client.`);

            balance = Number(readline.question("How much cash do you want to deposit to get started with your account? (10€ is the minimum)"));

            while (!balance || balance < 10) {
                balance = Number(readline.question("Unfortunately we can't open an account for such a small account. Do you have any more cash with you?"));
            }

            let data;

            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            const id = data.length > 0 ? data.length + 1 : 1;

            console.log(`Great ${name}! You now have an account (ID: ${id}) with balances of ${balance}€`)

            const password = readline.question("We're happy to have you as a customer, and we want to ensure that your money is safe with us. Give us a password, which gives only you the access to your account.");

            const account = {
                id,
                name,
                balance,
                password,
                fundRequests: []
            };

            data.push(account);

            try {
                saveData(JSON.stringify(data));
                console.log("Account saved");
                running = false;
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }
        }
    },

    closeAccount: () => {
        let running = true;

        if (!logged) {
            console.log("Not logged in");
            running = false;
        }

        while (running) {

            console.log("So you wan't to close your account, huh?");

            let confirm = readline.question("Are you sure about it?").trim();

            if (confirm.toLocaleLowerCase() === "yes") {
                let data;

                try {
                    data = loadData();
                } catch (error) {
                    console.log("database error");
                    running = false;
                    return;
                }

                const newData = data.filter(account => account.id !== logged);

                try {
                    saveData(JSON.stringify(newData));
                    console.log("Okay, sad to see you leave. We've now closed your account.");
                    logged = null;
                    running = false;
                } catch (error) {
                    console.log("database error");
                    running = false;
                    return;
                }
            }

            console.log("Alright, aborting the command.");
            running = false;
        }
    },

    withdrawFunds: () => {
        let running = true;
        let password;
        let data;
        let amount;
        let account;
        let id;

        while (running) {
            console.log("Okay, let's whip up some cash for you from these ones and zeroes.");

            id = readline.question("What is your account ID?").trim();

            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            account = data.find(account => account.id === Number(id));

            while (!account) {
                id = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again.").trim();
                account = data.find(account => account.id === Number(id));
            }

            password = readline.question("Okay, we found an account with that ID. You will need to insert your password so we can validate it's actually you");

            while (!(password === account.password)) {
                password = readline.question("Ah, there must by a typo. Try typing it again.");
            }

            amount = Number(readline.question(`Awesome, we validated you ${account.name}! How much money do you want to withdraw? (Current balance:${account.balance}€)`));

            while (amount > account.balance) {
                amount = Number(readline.question("Unfortunately you don't have the balance for that. Let's try a smaller amount."));
            }

            account.balance -= amount;

            const index = data.findIndex(account => account.id === Number(id));

            data[index] = account;

            try {
                saveData(JSON.stringify(data));
                console.log(`Awesome, you can now enjoy your ${amount}€ in cash! There's still ${account.balance}€ in your account, safe with us.`);
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            running = false;
        }
    },

    depositFunds: () => {
        let running = true;
        let password;
        let data;
        let amount;
        let account;
        let id;

        while (running) {
            console.log("Okay, let's convert your cash in to some delicious ones and zeroes, then feed them in to your hungry system.");

            id = readline.question("What is your account ID?").trim();

            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            account = data.find(account => account.id === Number(id));

            while (!account) {
                id = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again.").trim();
                account = data.find(account => account.id === Number(id));
            }

            password = readline.question("Okay, we found an account with that ID. You will need to insert your password so we can validate it's actually you");

            while (!(password === account.password)) {
                password = readline.question("Ah, there must by a typo. Try typing it again.");
            }

            amount = Number(readline.question(`Awesome, we validated you ${account.name}! How much money do you want to deposit? (Current balance:${account.balance}€)`));

            account.balance += amount;

            const index = data.findIndex(account => account.id === Number(id));

            data[index] = account;

            try {
                saveData(JSON.stringify(data));
                console.log(`Awesome, we removed ${amount}€ from existence and stored them in to our system. Now your accounts balance is ${account.balance}€`);
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            running = false;
        }
    },

    transferFunds: () => {
        console.log("Okay, let's slide these binary treats in to someone elses pockets.");
        let password;
        let data;
        let amount;
        let account;
        let id;
        let tranferID;
        let transferAccount;

        let running = true;

        while (running) {
            id = readline.question("Let's start with your account ID.").trim();

            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            account = data.find(account => account.id === Number(id));

            while (!account) {
                id = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?").trim();
                account = data.find(account => account.id === Number(id));
            }

            password = readline.question("Okay, we found an account with that ID. You will need to insert your password so we can validate it's actually you");

            while (!(password === account.password)) {
                password = readline.question("Ah, there must by a typo. Try typing it again.");
            }

            amount = Number(readline.question(`Awesome, we validated you ${account.name}! How much money do you want to transfer? (Current balance:${account.balance}€)`));

            while (amount > account.balance) {
                amount = Number(readline.question("Unfortunately you don't have the balance for that. Let's try a smaller amount."));
            }

            tranferID = readline.question("Awesome, we can do that. What is the ID of the account you want to transfer these funds into?").trim();

            transferAccount = data.find(account => account.id === Number(tranferID));

            while (!transferAccount) {
                tranferID = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?").trim();
                transferAccount = data.find(account => account.id === Number(tranferID));
            }
            
            data[data.findIndex(account => account.id === Number(id))].balance -= amount;
            data[data.findIndex(account => account.id === Number(tranferID))].balance += amount;

            try {
                saveData(JSON.stringify(data));
                console.log(`Awesome. We sent ${amount} euros to an account with the ID of ${tranferID}.`);
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            running = false;
        }
    },
    
    doesAccountExist: () => {
        let data;

        try {
            data = loadData();
        } catch (error) {
            console.log("database error");
            running = false;
            return;
        } 
        
        const id = readline.question("Mhmm, you want to check if an account with an ID exists. Let's do it! Give us the ID and we'll check.").trim();

        if (data.some(account => account.id === Number(id))) {
            return console.log("Awesome! This account actually exists. You should confirm with the owner that this account is actually his.");
        } 

        return console.log("Mhmm, unfortunately an account with that ID does not exist.");
    },

    modifyAccount: () => {
        let running = true;
        let id;
        let account
        let data;
        let password;
        let newName;

        while (running){
            id = readline.question("Mhmm, you want to modify an accounts stored holder name. We can definitely do that! Let's start validating you with your ID!").trim();
            
            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            account = data.find(account => account.id === Number(id));

            while (!account) {
                id = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?").trim();
                account = data.find(account => account.id === Number(id));
            }

            password = readline.question("Okay, we found an account with that ID. You will need to insert your password so we can validate it's actually you.");

            while (!(password === account.password)) {
                password = readline.question("Ah, there must by a typo. Try typing it again.");
            }

            newName = readline.question(`Awesome, we validated you ${account.name}! What is the new name for the account holder?`);

            while (newName === account.name) {
                newName = readline.question(`hmm, I'm quite sure this is the same name. Try typing it out again.`);
            }

            data[data.findIndex(account => account.id === Number(id))].name = newName;

            try {
                saveData(JSON.stringify(data));
                console.log(`Ah, there we go. We will address you as ${newName} from now on.)`);
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            running = false;
        }
    },

    login: () => {
        let running = true;
        let id;
        let account
        let data;
        let password;

        while (running) {
            id = readline.question("So you want to log in? Give us your ID.").trim();

            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            account = data.find(account => account.id === Number(id));

            while (!account) {
                id = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?").trim();
                account = data.find(account => account.id === Number(id));
            }

            password = readline.question("Okay, we found an account with that ID. You will need to insert your password so we can validate it's actually you.");

            while (!(password === account.password)) {
                password = readline.question("Ah, there must by a typo. Try typing it again.");
            }

            logged = account.id;

            console.log(`Awesome, we validated you ${account.name}! You are now logged in`);
            running = false;
        }
    },

    logout: () => {
        let running = true;

        if (!logged) {
            console.log("No user is currently logged in");
            running = false;
        }

        while (running) {
            let confirmation = readline.question("Are you sure you wish to logout?").trim();

            if (confirmation.toLowerCase() === "yes") {
                logged = null;
            }

            running = false;
        }
    },

    requestFunds: () => {
        let running = true;
        let id;
        let amount;
        let data;
        let account;

        if (!logged) {
            console.log("Not logged in");
            running = false;
        }

        while (running) {
            id = readline.question("So you want request funds from someone? Give us their ID.").trim();

            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            account = data.find(account => account.id === Number(id));

            while (!account) {
                id = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?").trim();
                account = data.find(account => account.id === Number(id));
            }

            amount = Number(readline.question(`Okay, we found an account with that ID. How much money do you want to request?`));

            data[data.findIndex(account => account.id === Number(id))].fundRequests.push({id:logged, amount});

            try {
                saveData(JSON.stringify(data));
                console.log(`Awesome! We'll request that amount from the user with ID ${id}.`);
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            running = false;
        }
    },

    fundsRequests: () => {

    },

    acceptFundRequest: () => {
        let running = true;
        let id;
        let data;
        let requests;
        let account;

        if (!logged) {
            console.log("Not logged in");
            running = false;
        }

        while (running) {
            id = readline.question("So you want to accept someones fund request? Give us their ID.").trim();
            
            try {
                data = loadData();
            } catch (error) {
                console.log("database error");
                running = false;
                return;
            }

            account = data.find(account => account.id === Number(logged));
            requests = account.fundRequests.filter(request => request.id === Number(id));

            while (!(requests.length > 0)) {
                id = readline.question("Mhmm, unfortunately there's no request for your funds with that account ID. Try again?").trim();
                requests = account.fundRequests.filter(request => request.id === Number(id));
            }

            const total = requests.reduce((a, b) => a + b.amount, 0);

            const confirm = readline.question(`Okay, we found a request for your funds of ${total} euros. Type yes to accept this request.`).trim();

            if (confirm.toLocaleLowerCase() === "yes") {
                if (account.balance < total) {
                    console.log("Unfortunately you don't have the funds for a request like this!");                    
                } else {
                    account.balance -= total;
                    account.fundRequests = account.fundRequests.filter(request => request.id !== Number(id));

                    data[data.findIndex(account => account.id === Number(logged))] = account;
                    data[data.findIndex(account => account.id === Number(id))].balance += total;

                    try {
                        saveData(JSON.stringify(data));
                        console.log(`Awesome! We'll request that amount from the user with ID ${id}.`);
                    } catch (error) {
                        console.log("database error");
                        running = false;
                        return;
                    }
                }
            }

            running = false;
        }
    }
}

export default commands;