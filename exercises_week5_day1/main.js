import readline from "readline-sync";
import commands from "./commands.js";

console.log("Welcome to ________ banking CLI!");

while (true) {
    console.log("You can get help with the commands by typing “help”.");
    const input = readline.question(">>").trim();

    if (input in commands) {
        commands[input]();
    } else {
        console.log("Invalid input");
    }    
}

